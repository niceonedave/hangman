Feature: Hangman

Scenario: Guess a correct letter
  Given I start a game of hangman with answer "diesel"
   When I guess the letter "d"
   Then letters revealed are "d*****"
    And "0" lives are lost
    And incorrect letters guessed are ""
    And game is in progress

Scenario: Guess an incorrect letter
 Given I start a game of hangman with answer "diesel"
 When I guess the letter "t"
 Then letters revealed are "******"
 And "1" life is lost
 And incorrect letters guessed are "t"
 And game is in progress

 Scenario: All 6 letters are incorrect
  Given I start a game of hangman with answer "diesel"
  When I guess the letters "a,b,c,f,j,m"
  Then letters revealed are "******"
  And "6" lives are lost
  And incorrect letters guessed are "a,b,c,f,j,m"
  And game is lost


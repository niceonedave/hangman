package com.verint;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class StepDefinitions {

	private Hangman hangman;

	@Given("^I start a game of hangman with answer \"([^\"]*)\"$")
	public void startAGame(String answer) {
		hangman = new Hangman(answer);
	}

	@When("^I guess the letter \"([^\"]*)\"$")
	public void guessTheLetter(String letter) {
		hangman.guessLetter(letter);
	}

	@Then("^letters revealed are \"([^\"]*)\"$")
	public void lettersRevealedAre(String lettersRevealed) {
		assertThat(hangman.getRevealedLetters(), is(lettersRevealed));
	}

	@And("^\"([^\"]*)\" (?:lives are|life is) lost$")
	public void livesAreLost(int lives) {
		assertThat(hangman.getLivesLost(), is(lives));
	}

	@And("^incorrect letters guessed are \"([^\"]*)\"$")
	public void incorrectLettersGuessedAre(String letters) {
		if (letters.isEmpty()) {
			assertThat(hangman.getIncorrectLetters(), is(empty()));
		} else {
			assertThat(hangman.getIncorrectLetters(), contains(letters.split(",")));
		}
	}

	@And("^game is in progress$")
	public void gameIsInProgress() {
		assertTrue(hangman.isGameInProgress());
	}

	@When("^I guess the letters \"([^\"]*)\"$")
	public void guessTheLetters(String letters) {
		for (String letter : letters.split(",")) {
			hangman.guessLetter(letter);
		}
	}

	@And("^game is lost$")
	public void gameIsLost() {
		assertTrue(hangman.isGameLost());
	}
}
